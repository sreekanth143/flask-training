from flask import Flask
app = Flask(__name__)


@app.route('/')
def hello_world():
    return 'Hello Sreekanth'


if __name__ == '__main__':
    app.run()
# from werkzeug.utils import secure_filename
# from flask import Request
# from flask import render_template
# from flask import url_for
# from markupsafe import escape
# from flask import Flask
# from flask import request
# app = Flask(__name__)

# @app.route("/")
# def hello_world():
#     return "<p>Hello World</p>"


# # @app.route('/hello/<name>')
# # def hello_name(name):
# #     return 'Hello %s!' % name

# # HTML Escaping
# @app.route("/<name>")
# def hello(name):
#     return f"Hello, {escape(name)}!"


# # Routing
# app.route('/')


# def index():
#     return 'Index page'


# app.route('/hello')


# def hello():
#     return 'Hello, world'


# @app.route('/user/<username>')
# def show_user_profile(username):
#     # show the user profile for that user
#     return f'User {escape(username)}'


# @app.route('/post/<int:post_id>')
# def show_post(post_id):
#     # show the post with the given id, the id is an integer
#     return f'Post {post_id}'


# @app.route('/path/<path:subpath>')
# def show_subpath(subpath):
#     # show the subpath after /path/
#     return f'Subpath {escape(subpath)}'


# # Unique URLs / Redirection Behavior
# @app.route('/projects/')
# def projects():
#     return 'The project page'


# @app.route('/about')
# def about():
#     return 'The about page'


# # url building


# @app.route('/')
# def index():
#     return 'index'


# @app.route('/login')
# def login():
#     return 'login'


# @app.route('/user/<username>')
# def profile(username):
#     return f'{username}\'s profile'


# with app.test_request_context():
#     print(url_for('index'))
#     print(url_for('login'))
#     print(url_for('login', next='/'))
#     print(url_for('profile', username='John Doe'))
#     print(url_for('static', filename='style.css'))

# # HTTP methods


# # @app.route('/signing', methods=['GET', 'POST'])
# # def login():
# #     if requested.method == 'POST':
# #         return do_the_login()
# #     else:
# #         return Show_the_login_form()


# @app.get('/login')
# def login_get():
#     return show_the_login_form()


# @app.post('/login')
# def login_post():
#     return do_the_login()


# # rendering Templates


# @app.route('/hello/')
# @app.route('/hello/<name>')
# def html(name=None):
#     return render_template('hello.html', name=name)


# with app.request_context(environ):
#     assert request.method == 'POST'

# @app.route('/login', methods=['POST', 'GET'])
# def login():
#     error = None
#     if request.method == 'POST':
#         if valid_login(request.form['username'],
#                        request.form['password']):
#             return log_the_user_in(request.form['username'])
#         else:
#             error = 'Invalid username/password'
#     # the code below is executed if the request method
#     # was GET or the credentials were invalid
#     return render_template('hello.html', error=error)


# # searchword = request.args.get('key', '')

# # File uploads


# @app.route('/update', methods=['GET', 'POST'])
# def upload_file():
#     if request.method == 'POST':
#         f = request.files['the_file']
#         f.save(f"/var/www/uploads/{secure_filename(f.filename)}")


# if __name__ == '__main__':
#     app.run(debug=True)


# import os
# from flask import Flask, flash, request, redirect, url_for
# from werkzeug.utils import secure_filename

# UPLOAD_FOLDER = '/path/to/the/uploads'
# ALLOWED_EXTENSIONS = {'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'}

# app = Flask(__name__)
# app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


# def allowed_file(filename):
#     return '.' in filename and \
#         filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


# @app.route('/', methods=['GET', 'POST'])
# def upload_file():
#     if request.method == 'POST':
#         if 'file' not in request.files:
#             flash('NO file part')
#             return redirect(request.url)
#         file = request.files['file']
#         if file.filename == '':
#             flash('NO selected file')
#             return redirect(request.url)
#         if file and allowed_file(file.filename):
#             filename = secure_filename(file.filename)
#             file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
#             return redirect(url_for('download_file', name=filename))

#     return '''
# <!doctype html>
# <title>UPLOAD NEW FILE</TITLE>
# <h1>Upload new File</h1>
# <form method = post enctype = multipart/form-data>
# <input type = file name=file><input type =submit value=Upload>
# </form>'''

# reading cookies
# from flask import Flask, make_response, render_template, request


# @app.route('/')
# def index():
#     username = request.cookies.get('username')


# # Storing cookies


# @app.route('/')
# def index():
#     resp = make_response(render_template(...))
#     resp.set_cookie('username', 'the username')
#     return resp

# return Redirects and errors
# from flask import Flask, abort, redirect, url_for
# app = Flask(__name__)


# @app.route('/')
# def index():
#     return redirect(url_for('login'))


# @app.route('/login')
# def login():
#     abort(401)
#     this_is_never_executed()

# from flask import session
# from flask import Flask
# from flask import render_template
# from flask import Flask, abort, redirect, url_for
# app = Flask(__name__)


# @app.route('/')
# def index():
#     return redirect(url_for('login'))


# @app.route('/login')
# def login():
#     abort(401)
#     this_is_never_executed()


# @app.errorhandler(404)
# def page_not_found(error):
#     return render_template('page_not_found.html'), 404

# @app.route("/me")
# def me_api():
#     user = get_current_user()
#     return {
#         "username": user.username,
#         "theme": user.theme,
#         "image": url_for("user_image", filename=user.image),
#     }


# @app.route("/users")
# def users_api():
#     users = get_all_users()
#     return [user.to_json() for user in users]


# Set the secret key to some random bytes. Keep this really secret!
# app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'


# @app.route('/')
# def index():
#     if 'username' in session:
#         return f'Logged in as {session["username"]}'
#     return 'You are not logged in'


# @app.route('/login', methods=['GET', 'POST'])
# def login():
#     if request.method == 'POST':
#         session['username'] = request.form['username']
#         return redirect(url_for('index'))
#     return '''
#         <form method="post">
#             <p><input type=text name=username>
#             <p><input type=submit value=Login>
#         </form>
#     '''


# @app.route('/logout')
# def logout():
#     # remove the username from the session if it's there
#     session.pop('username', None)
#     return redirect(url_for('index'))from flask import session


# # Set the secret key to some random bytes. Keep this really secret!
# app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'


# @app.route('/')
# def index():
#     if 'username' in session:
#         return f'Logged in as {session["username"]}'
#     return 'You are not logged in'


# @app.route('/login', methods=['GET', 'POST'])
# def login():
#     if request.method == 'POST':
#         session['username'] = request.form['username']
#         return redirect(url_for('index'))
#     return '''
#         <form method="post">
#             <p><input type=text name=username>
#             <p><input type=submit value=Login>
#         </form>
#     '''


# @app.route('/logout')
# def logout():
#     # remove the username from the session if it's there
#     session.pop('username', None)
#     return redirect(url_for('index'))
